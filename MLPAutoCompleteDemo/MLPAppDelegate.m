/************************************************************
 *  * HongHe CONFIDENTIAL
 * __________________
 * Copyright (C) 2013-2014 HongHe Technologies. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of HongHe Technologies.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from HongHe Technologies.
 *
 * project created by knight 349919133
 *
 */

#import "MLPAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "MLPViewController.h"

@implementation MLPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[MLPViewController alloc] initWithNibName:@"View" bundle:[NSBundle mainBundle]];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    [self.window.layer setCornerRadius:4];
    [self.window.layer setMasksToBounds:YES];
    [self.window setBackgroundColor:self.viewController.view.backgroundColor];
    return YES;
}

@end
