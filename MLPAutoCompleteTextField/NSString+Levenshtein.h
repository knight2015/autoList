/************************************************************
 *  * HongHe CONFIDENTIAL
 * __________________
 * Copyright (C) 2013-2014 HongHe Technologies. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of HongHe Technologies.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from HongHe Technologies.
 *
 * project created by knight 349919133
 *
 */


#import <Foundation/Foundation.h>
#import <float.h>

// used to indicate that stringB is nil
#define LEV_INF_DISTANCE FLT_MAX

@interface NSString(Levenshtein)

- (float) asciiLevenshteinDistanceWithString: (NSString *)stringB;
- (float) asciiLevenshteinDistanceWithString: (NSString *)stringB skippingCharacterSet: (NSCharacterSet *)charset;

@end